﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyDLL
{
    public partial class AccountInfo
    {
        [JsonProperty("Response")]
        public Response Response { get; set; }
    }

    public partial class Response
    {
        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("login")]
        public string Login { get; set; }
    }
}
