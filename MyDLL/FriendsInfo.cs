﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyDLL
{
    public partial class FriendsInfo
    {
        [JsonProperty("Response")]
        public ResponseFriendsInfo Response { get; set; }
    }

    public partial class ResponseFriendsInfo
    {
        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("FriendsInfo")]
        public FriendsInfoElement[] FriendsInfo { get; set; }
    }

    public partial class FriendsInfoElement
    {
        [JsonProperty("Name")]
        public string Name { get; set; }
    }
}
