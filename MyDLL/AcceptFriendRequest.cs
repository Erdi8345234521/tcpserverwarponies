﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyDLL
{
    public partial class AcceptFriendRequest
    {
        [JsonProperty("Response")]
        public ResponseAcceptFriendRequest Response { get; set; }
    }

    public partial class ResponseAcceptFriendRequest
    {
        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("WhoAcceptedTheApplication")]
        public string WhoAcceptedTheApplication { get; set; }

        [JsonProperty("SomeoneMadeARequest")]
        public string SomeoneMadeARequest { get; set; }
    }
}
