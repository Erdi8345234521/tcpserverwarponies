﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace MyDLL
{
    public partial class PlayerInfo
    {
        [JsonProperty("Response")]
        public ResponsePlayerInfo Response { get; set; }
    }

    public partial class ResponsePlayerInfo
    {
        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("IsCreatedCharacter")]
        public string IsCreatedCharacter { get; set; }

        [JsonProperty("PlayerName")]
        public string PlayerName { get; set; }

        [JsonProperty("Experience")]
        public string Experience { get; set; }

        public string Sex { get; set; }

        [JsonProperty("visual_information")]
        public VisualInformation VisualInformation { get; set; }
    }

    public partial class VisualInformation
    {
        [JsonProperty("ColorBody")]
        public string ColorBody { get; set; }

        [JsonProperty("ColorEye")]
        public string ColorEye { get; set; }

        [JsonProperty("ColorHairFront_1")]
        public string ColorHairFront1 { get; set; }

        [JsonProperty("ColorHairFront_2")]
        public string ColorHairFront2 { get; set; }

        [JsonProperty("ColorHairBack_1")]
        public string ColorHairBack1 { get; set; }

        [JsonProperty("ColorHairBack_2")]
        public string ColorHairBack2 { get; set; }

        [JsonProperty("ColorTail_1")]
        public string ColorTail1 { get; set; }

        [JsonProperty("ColorTail_2")]
        public string ColorTail2 { get; set; }

        [JsonProperty("ModelHairFront")]
        public string ModelHairFront { get; set; }

        [JsonProperty("ModelHairBack")]
        public string ModelHairBack { get; set; }

        [JsonProperty("ModelTail")]
        public string ModelTail { get; set; }
    }
}
