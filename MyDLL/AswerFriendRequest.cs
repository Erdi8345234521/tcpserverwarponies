﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace MyDLL
{
    public partial class AswerFriendRequest
    {
        [JsonProperty("Response")]
        public ResponseAswerFriendRequest Response { get; set; }
    }

    public partial class ResponseAswerFriendRequest
    {
        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("Aswer")]
        public string Aswer { get; set; }
    }
}
