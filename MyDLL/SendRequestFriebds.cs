﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyDLL
{
    public partial class SendRequestFriebds
    {
        [JsonProperty("Response")]
        public ResponseSendRequestFriebds Response { get; set; }
    }

    public partial class ResponseSendRequestFriebds
    {
        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("PlayerName")]
        public string PlayerName { get; set; }

        [JsonProperty("FromWhomCameTheRequest")]
        public string FromWhomCameTheRequest { get; set; }

        [JsonProperty("FromWhomCameTheRequestId")]
        public string FromWhomCameTheRequestId { get; set; }
    }
}
