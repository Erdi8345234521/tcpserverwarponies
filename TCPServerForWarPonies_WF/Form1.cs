﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace TCPServerForWarPonies_WF
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Program.f1 = this;
        }

        static ServerObject server; // сервер
        static Thread listenThread; // потока для прослушивания
        static GameServerRun GameServer;

        public void AddingRecordsFromRTB_LogsServer(string Text)
        {
            this.InvokeIfNeeded(() => RTB_Logs.Text += "Сервер: " + Text + "\n");
        }
        public void AddingRecordsFromRTB_LogsClient(string Text, string ID_Client)
        {
            this.InvokeIfNeeded(() => RTB_Logs.Text += "Клиент: " + Text + " Id клиента в базе данных: " + ID_Client + "\n");
        }

        public void AddingRecordsFromRTB_LogsMS(string Text)
        {
            this.InvokeIfNeeded(() => RTB_LogsMS.Text += "Мастер-Сервер: " + Text + "\n");
        }
   
        private int port = 8888;
        private int runservercount = 0;
        private void B_RunGameServer_Click(object sender, EventArgs e)
        {
            GameServer = new GameServerRun();
            GameServer.RunServer(port);
            port++;
            runservercount++;
            L_CountRunServer.Text = string.Format("Кол-во зарущенных серверов: {0}", runservercount);
        }

        private void B_ServerStart_Click(object sender, EventArgs e)
        {
            if (B_ServerStart.Text == "Старт Сервер")
            {
                try
                { 
                    server = new ServerObject();
                    listenThread = new Thread(new ThreadStart(server.Listen));
                    listenThread.Start(); //старт потока
                    B_ServerStart.Text = "Стоп Сервер";
                }
                catch (Exception ex)
                {
                    server.Disconnect();
                    RTB_Logs.Text += ex.Message;
                }
            }
            else
            {
                B_ServerStart.Text = "Старт Сервер";
            }
        }

        private void B_SaveLogs_Click(object sender, EventArgs e)
        {
            using (FileStream fstream = new FileStream(@"E:\BECUP\note.txt", FileMode.OpenOrCreate))
            {
                // преобразуем строку в байты
                byte[] array = System.Text.Encoding.Default.GetBytes(RTB_Logs.Text);
                // запись массива байтов в файл
                fstream.Write(array, 0, array.Length);
                Console.WriteLine("Текст записан в файл");
            }
        }

        private void B_ClearLogs_Click(object sender, EventArgs e)
        {
            RTB_Logs.Text = "";
        }
    }  
}
