﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.ComponentModel;
using System.Windows.Forms;

namespace TCPServerForWarPonies_WF
{
    class ServerObject
    {
        static TcpListener tcpListener; // сервер для прослушивания
        List<ClientObject> clients = new List<ClientObject>(); // все подключения

        protected internal void AddConnection(ClientObject clientObject)
        {
            clients.Add(clientObject);
        }
        protected internal void RemoveConnection(string id)
        {
            // получаем по id закрытое подключение
            ClientObject client = clients.FirstOrDefault(c => c.Id == id);
            // и удаляем его из списка подключений
            if (client != null)
                clients.Remove(client);
        }
        // прослушивание входящих подключений
        protected internal void Listen()
        {
            try
            {

                tcpListener = new TcpListener(IPAddress.Parse("26.133.136.203"), Convert.ToInt32(Program.f1.TB_PortDataServer.Text));
                //tcpListener.Server.Listen(10);
                tcpListener.Start();
                
                Program.f1.AddingRecordsFromRTB_LogsServer("Сервер запущен. Ожидание подключений...");

                while (true)
                {
                    TcpClient tcpClient = tcpListener.AcceptTcpClient();

                    ClientObject clientObject = new ClientObject(tcpClient, this);
                    Thread clientThread = new Thread(new ThreadStart(clientObject.Process));
                    clientThread.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Disconnect();
            }
        }

        // трансляция сообщения подключенным клиентам
        protected internal void BroadcastMessage(string message, string id)
        {
            byte[] data = Encoding.Unicode.GetBytes(message);
            for (int i = 0; i < clients.Count; i++)
            {
                if (clients[i].Id != id) // если id клиента не равно id отправляющего
                {
                    clients[i].Stream.Write(data, 0, data.Length); //передача данных
                }
            }
        }

        // трансляция сообщения только клиенту

        protected internal void castMessage(string message,string id)
        {
            byte[] data = Encoding.UTF8.GetBytes(message);
            for (int i = 0; i < clients.Count; i++)
            {
                if (clients[i].Id == id) // если id клиента равно id отправляющего
                {
                    clients[i].Stream.Write(data, 0, data.Length); //передача данных
                }
                
            }
        }
        protected internal void castMessageById(string message, string id)
        {
            byte[] data = Encoding.UTF8.GetBytes(message);
            for (int i = 0; i < clients.Count; i++)
            {                
                if (clients[i].Id.Contains(id)) // если id клиента равно id отправляющего
                {
                    clients[i].Stream.Write(data, 0, data.Length); //передача данных
                    Program.f1.AddingRecordsFromRTB_LogsServer("Id найден: " + id);
                }
            }
        }
        // отключение всех клиентов
        protected internal void Disconnect()
        {
            tcpListener.Stop(); //остановка сервера

            for (int i = 0; i < clients.Count; i++)
            {
                clients[i].Close(); //отключение клиента
            }
            Environment.Exit(0); //завершение процесса
        }
    }
    public static class ControlExtentions
    {
        public static void InvokeIfNeeded(this Control control, Action doit)
        {

            if (control.InvokeRequired)

                control.Invoke(doit);

            else

                doit();

        }
        public static void InvokeIfNeeded<T>(this Control control, Action<T> doit, T arg)
        {

            if (control.InvokeRequired)

                control.Invoke(doit, arg);

            else

                doit(arg);
        }
    }
}
