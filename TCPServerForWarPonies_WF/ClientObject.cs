﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Data.Common;
using CryptSharp;
using System.Threading;
using System.IO;
using MyDLL;

namespace TCPServerForWarPonies_WF
{

    class ClientObject
    {
        protected internal string Id { get; private set; }
        protected internal NetworkStream Stream { get; private set; }

        TcpClient client;
        ServerObject server; // объект сервера

        private string ip = "26.133.136.203";

        public ClientObject(TcpClient tcpClient, ServerObject serverObject)
        {
            Id = Guid.NewGuid().ToString();
            client = tcpClient;
            server = serverObject;
            serverObject.AddConnection(this);
            
        }

        private  string temporal_informationJSON = string.Empty;

        public void Process()
        {
            //try
            //{

                // получаем имя пользователя
                string message = string.Empty;
                string id_BD_User = string.Empty;
                string mess = string.Empty;

                //Для подключение к БД

                string serverName = "26.140.27.114"; // Адрес сервера (для локальной базы пишите "localhost")
                string userName = "root"; // Имя пользователя
                string dbName = "warponies"; //Имя базы данных
                string port = "3306"; // Порт для подключения
                string password = ""; // Пароль для подключения
                string connStr = "server=" + serverName +
                ";user=" + userName +
                ";database=" + dbName +
                ";port=" + port +
                ";password=" + password + ";";

                //---------------------

                Program.f1.AddingRecordsFromRTB_LogsClient("Клиент подключен--->",id_BD_User);
                Program.f1.AddingRecordsFromRTB_LogsClient("Внутренний ID клиента: " + Id, id_BD_User);


                //Program.f1.AddingRecordsFromRTB_LogsClient("Таймаут подключения к серверу: " + connection.ConnectionTimeout);
                Stream = client.GetStream();
            // в бесконечном цикле получаем сообщения от клиента
            while (true)
            {
                //try
                //{
                //Поулчаем данные
                byte[] data = new byte[64]; // буфер для получаемых данных
                StringBuilder builder = new StringBuilder();
                int bytes = 0;
                do
                {
                    bytes = Stream.Read(data, 0, data.Length);
                    builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
                }
                while (Stream.DataAvailable);//Пока в потоке есть данные

                message = builder.ToString();
                mess = builder.ToString();
                var Response = JObject.Parse(message);
                message = Response["Response"]["Type"].ToString();
                switch (message)
                {

                    //Приходят данные аккаунта игрока,после проверки,овтет отправляется обратно на клиент 
                    case "Account Data Verification":
                        using (MySqlConnection connectionAccountDataVerification = new MySqlConnection(connStr))
                        {
                            connectionAccountDataVerification.Open();
                            Program.f1.AddingRecordsFromRTB_LogsClient("Все работает!", id_BD_User);
                            AccountInfo accountInfo = JsonConvert.DeserializeObject<AccountInfo>(mess);
                            //Program.f1.AddingRecordsFromRTB_Logs(accountInfo.Response.Login);
                            string sql = "SELECT `id`, `login`,`password` FROM `usersinfo` WHERE login = '" + accountInfo.Response.Login + "'"; // Строка запроса
                            MySqlCommand sqlCom = new MySqlCommand(sql, connectionAccountDataVerification);
                            using (MySqlDataReader reader = sqlCom.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        WebRequest request = WebRequest.Create("http://" + ip + "/PasswordCheck.php");
                                        request.Method = "POST";
                                        string data1 = "password=" + accountInfo.Response.Password + "&id=" + reader.GetString(0);

                                        byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(data1);

                                        request.ContentType = "application/x-www-form-urlencoded";

                                        request.ContentLength = byteArray.Length;


                                        using (Stream dataStream = request.GetRequestStream())
                                        {
                                            dataStream.Write(byteArray, 0, byteArray.Length);
                                        }

                                        WebResponse response = request.GetResponse();

                                        using (Stream stream1 = response.GetResponseStream())
                                        {
                                            using (StreamReader reader1 = new StreamReader(stream1))
                                            {
                                                string potoc = reader1.ReadToEnd();
                                                //Console.WriteLine(potoc);
                                                //Console.WriteLine("лол");
                                                //Console.WriteLine(potoc);
                                                if (potoc == "Password_verification_failed")
                                                {
                                                    Program.f1.AddingRecordsFromRTB_LogsClient("Не удалось верефицировать пароль пользователя", id_BD_User);
                                                    //Ошибка авторизации
                                                    message = "Password_verification_failed";
                                                    server.castMessage(message, id: Id);

                                                }
                                                else
                                                {
                                                    if (reader.GetString(1) == accountInfo.Response.Login)
                                                    {
                                                        id_BD_User = reader.GetString(0);
                                                        Id = id_BD_User;
                                                        message = "Password_verification_True";
                                                        server.castMessage(message, Id);
                                                    }
                                                    else
                                                    {
                                                        message = "Password_verification_failed";
                                                        server.castMessage(message, Id);
                                                    }
                                                    Program.f1.AddingRecordsFromRTB_LogsClient("Пароль пользователя успешно верефицирован--->\nПропускаем дальше-->", id_BD_User);
                                                }
                                            }
                                        }

                                        //Console.WriteLine(reader.GetString(1));
                                        //Console.WriteLine("id" + reader.GetString(0));
                                        Program.f1.AddingRecordsFromRTB_LogsClient("Запрос выполнен...", id_BD_User);
                                    }
                                }
                            }
                        }
                        break;

                    //Установка данных персонажа (данные приходят с сервера,после чего записываются в БД)
                    case "SetPlayerData":
                        using (MySqlConnection connectionSetPlayerData = new MySqlConnection(connStr))
                        {
                            connectionSetPlayerData.Open();

                            //Program.f1.AddingRecordsFromRTB_LogsClient("Таймаут подключения к серверу: " + connection.ConnectionTimeout);
                            PlayerInfo playerInfo = JsonConvert.DeserializeObject<PlayerInfo>(mess);

                            string SetPlayerData = "UPDATE `playersinfo` SET `IsCreatedCharacter`= 1,`NickName` = " +
                               "'" + playerInfo.Response.PlayerName + "'" +
                               " WHERE UserInfoId = " + id_BD_User;
                            string GetCountItem = "SELECT count(*) FROM `gameitemsinfo` WHERE `AccessibilityMethod` = 0";

                            //Отправление предметов на сервер
                            MySqlCommand sqlcom5 = new MySqlCommand(GetCountItem, connectionSetPlayerData);
                            int Count = Convert.ToInt32(sqlcom5.ExecuteScalar().ToString());
                            MySqlCommand mySqlCommand = new MySqlCommand("SELECT `ItemId` FROM `gameitemsinfo` WHERE `AccessibilityMethod` = 0", connectionSetPlayerData);
                            using (MySqlConnection connectionSetPlayerData_1 = new MySqlConnection(connStr))
                            {
                                connectionSetPlayerData_1.Open();
                                using (MySqlDataReader reader = mySqlCommand.ExecuteReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            string isf = reader.GetString(0);
                                            MySqlCommand sqlcom3 = new MySqlCommand("INSERT INTO `useritemsinfo` (`ItemId`, `UserId`) VALUES ('" + isf + "','" + id_BD_User + "')", connectionSetPlayerData_1);
                                            sqlcom3.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }
                            //Отправление предметов на сервер

                            
                            //Отправление цвета в базу данных
                            string sql2 = "INSERT INTO `usercolotinfo` (`ColorBody`,`ColorEye`,`ColorHairFront_1`,`ColorHairFront_2`," +

                                "`ColorHairBack_1`,`ColorHairBack_2`,`ColorTail_1`,`ColorTail_2`, `UserInfoId`) VALUES ('" + playerInfo.Response.VisualInformation.ColorBody + "','" + playerInfo.Response.VisualInformation.ColorEye + 
                                "','" + playerInfo.Response.VisualInformation.ColorHairFront1 + "','"+playerInfo.Response.VisualInformation.ColorHairFront2 +
                                "','" + playerInfo.Response.VisualInformation.ColorHairBack1 + "','" + playerInfo.Response.VisualInformation.ColorHairBack2 +
                                "','" + playerInfo.Response.VisualInformation.ColorTail1 + "','" + playerInfo.Response.VisualInformation.ColorTail2 + "','" + id_BD_User +"')";

                            MySqlCommand sqlcom6 = new MySqlCommand(sql2, connectionSetPlayerData);
                            sqlcom6.ExecuteNonQuery();

                            MySqlCommand sqlcom2 = new MySqlCommand(SetPlayerData, connectionSetPlayerData);
                            //connection.Ping();
                            sqlcom2.CommandTimeout = 60000;
                            sqlcom2.ExecuteNonQuery();
                            Program.f1.AddingRecordsFromRTB_LogsClient("Установка данных о игроке", id_BD_User);
                            message = "Successful character creation";
                            server.castMessage(message, Id);
                        }
                        break;

                    case "GetCharacterCreated":
                        using (MySqlConnection connectionGetCharacterCreated = new MySqlConnection(connStr))
                        {
                            connectionGetCharacterCreated.Open();
                            string sql1 = "SELECT `IsCreatedCharacter` FROM `playersinfo` WHERE UserInfoId = '" + id_BD_User + "'";
                            MySqlCommand sqlcom1 = new MySqlCommand(sql1, connectionGetCharacterCreated);
                            using (MySqlDataReader reader = sqlcom1.ExecuteReader())
                            {
                                string IsCreatedCharacter = string.Empty;
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        IsCreatedCharacter = reader.GetString(0);
                                        break;
                                    }
                                }
                                Program.f1.AddingRecordsFromRTB_LogsClient(IsCreatedCharacter, id_BD_User);
                                if (IsCreatedCharacter == "True")
                                {
                                    message = "CharacterCreatedTrue";
                                }
                                else if (IsCreatedCharacter == "False")
                                {
                                    message = "CharacterCreatedFalse";
                                }
                                else
                                {
                                    message = "ResponseError";
                                }
                                
                                server.castMessage(message, Id);
                            }
                            break;
                        }

                    case "GetPlayerDataInfo":
                        using (MySqlConnection connectionGetPlayerDataInfo = new MySqlConnection(connStr))
                        {
                            connectionGetPlayerDataInfo.Open();
                            Program.f1.AddingRecordsFromRTB_LogsClient("ID пользователя в Базе данных: " + id_BD_User, id_BD_User);

                            GetPlayerInfo getPlayerInfo = new GetPlayerInfo();
                            getPlayerInfo.Response = new ResponseGetPlayerInfo();
                            getPlayerInfo.Response.Type = "GetPlayersInfo";
                            getPlayerInfo.Response.Name = "lol";
                            getPlayerInfo.Response.Sex = 1;
                            getPlayerInfo.Response.IsCreatedCharacter = true;

                            string OP = string.Empty;
                            string PlayerName = string.Empty;
                            string Experience = string.Empty;
                            string ColorBody = string.Empty;
                            string ColorEye = string.Empty;
                            string ColorHairFront_1 = string.Empty;
                            string ColorHairFront_2 = string.Empty;
                            string ColorHairBack_1 = string.Empty;
                            string ColorHairBack_2 = string.Empty;
                            string ColorTail_1 = string.Empty;
                            string ColorTail_2 = string.Empty;
                            string ModelHairFront = string.Empty;
                            string ModelHairBack = string.Empty;
                            string ModelTail = string.Empty;


                            //Получение цвета пользователя
                            string sql_1 = "SELECT `ColorBody`,`ColorEye`,`ColorHairFront_1`,`ColorHairFront_2`,`ColorHairBack_1`,`ColorHairBack_2`," +
                                "`ColorTail_1`,`ColorTail_2` FROM `usercolotinfo` WHERE UserInfoId = " + id_BD_User;
                            MySqlCommand sqlcom4 = new MySqlCommand(sql_1, connectionGetPlayerDataInfo);
                            using (MySqlDataReader reader = sqlcom4.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        ColorBody = reader.GetString(0);
                                        ColorEye = reader.GetString(1);
                                        ColorHairFront_1 = reader.GetString(2);
                                        ColorHairFront_2 = reader.GetString(3);
                                        ColorHairBack_1 = reader.GetString(4);
                                        ColorHairBack_2 = reader.GetString(5);
                                        ColorTail_1 = reader.GetString(6);
                                        ColorTail_2 = reader.GetString(7);

                                    }
                                }
                            }
                            getPlayerInfo.Response.VisualInformation = new VisualInformationGetPlayerInfo();
                            getPlayerInfo.Response.VisualInformation.ColorBody = ColorBody;
                            getPlayerInfo.Response.VisualInformation.ColorEye = ColorEye;
                            getPlayerInfo.Response.VisualInformation.ColorHairFront1 = ColorHairFront_1;
                            getPlayerInfo.Response.VisualInformation.ColorHairFront2 = ColorHairFront_2;
                            getPlayerInfo.Response.VisualInformation.ColorHairBack1 = ColorHairBack_1;
                            getPlayerInfo.Response.VisualInformation.ColorHairBack2 = ColorHairBack_2;
                            getPlayerInfo.Response.VisualInformation.ColorTail1 = ColorTail_1;
                            getPlayerInfo.Response.VisualInformation.ColorTail2 = ColorTail_2;
                            getPlayerInfo.Response.VisualInformation.ModelHairFront = ModelHairFront;
                            getPlayerInfo.Response.VisualInformation.ModelHairBack = ModelHairBack;
                            getPlayerInfo.Response.VisualInformation.ModelTail = ModelTail;

                            //Получение предметов пользователя

                            string sql_32 = "SELECT `ItemId` FROM `useritemsinfo` WHERE UserId = " + id_BD_User;
                            string sql_5 = "SELECT COUNT(*) FROM `useritemsinfo`"; // Строка запроса
                            MySqlCommand sqlcom5 = new MySqlCommand(sql_5, connectionGetPlayerDataInfo);
                            int Count = Convert.ToInt32(sqlcom5.ExecuteScalar().ToString());
                            MySqlCommand sqlcom6 = new MySqlCommand(sql_32, connectionGetPlayerDataInfo);
                            using (MySqlDataReader reader = sqlcom6.ExecuteReader())
                            {
                                getPlayerInfo.Response.ItemsId = new ItemsIdGetPlayerInfo[Count];
                                int x = 0;
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        getPlayerInfo.Response.ItemsId[x] = new ItemsIdGetPlayerInfo()
                                        {
                                            Id = reader.GetString(0)
                                        };
                                        x++;
                                    }
                                }
                            }

                            Program.f1.AddingRecordsFromRTB_LogsClient(JsonConvert.SerializeObject(getPlayerInfo), id_BD_User);


                            string sql_4 = "SELECT `Name` ,`UnicKey` FROM" +
                                " `itemsdata` WHERE UserInfoId = '" + id_BD_User + "'"; // Строка запроса

                            server.castMessage(JsonConvert.SerializeObject(getPlayerInfo), Id);

                            //Получение информации о пользователе












                            //string sql1 = "SELECT `id` ," +
                            //    "`IsCreatedCharacter`," +
                            //    "`NickName`," +
                            //    "`Experience`, " +
                            //    "`ColorCharctersBody`," +
                            //    "`ColorCharactersEye`," +
                            //    "`ColorHairFront_1`," +
                            //    "`ColorHairFront_2`," +
                            //    "`ColorHairBack_1`," +
                            //    "`ColorHairBack_2`," +
                            //    "`ColorTail_1`," +
                            //    "`ColorTail_2`," +
                            //    "`ModelHairFront`," +
                            //    "`ModelHairBack`," +
                            //    "`ModelTail` FROM `playersinfo` WHERE UserInfoId = '" + id_BD_User + "'"; // Строка запроса
                            //MySqlCommand sqlcom1 = new MySqlCommand(sql1, connectionGetPlayerDataInfo);
                            //using (MySqlDataReader reader = sqlcom1.ExecuteReader())
                            //{
                            //    string OP = string.Empty;
                            //    string PlayerName = string.Empty;
                            //    string Experience = string.Empty;
                            //    string ColorBody = string.Empty;
                            //    string ColorEye = string.Empty;
                            //    string ColorHairFront_1 = string.Empty;
                            //    string ColorHairFront_2 = string.Empty;
                            //    string ColorHairBack_1 = string.Empty;
                            //    string ColorHairBack_2 = string.Empty;
                            //    string ColorTail_1 = string.Empty;
                            //    string ColorTail_2 = string.Empty;
                            //    string ModelHairFront = string.Empty;
                            //    string ModelHairBack = string.Empty;
                            //    string ModelTail = string.Empty;

                            //    if (reader.HasRows)
                            //    {
                            //        while (reader.Read())
                            //        {
                            //            OP = reader.GetString(1);
                            //            PlayerName = reader.GetString(2);
                            //            Experience = reader.GetString(3);
                            //            ColorBody = reader.GetString(4);
                            //            ColorEye = reader.GetString(5);
                            //            ColorHairFront_1 = reader.GetString(6);
                            //            ColorHairFront_2 = reader.GetString(7);
                            //            ColorHairBack_1 = reader.GetString(8);
                            //            ColorHairBack_2 = reader.GetString(9);
                            //            ColorTail_1 = reader.GetString(10);
                            //            ColorTail_2 = reader.GetString(11);
                            //            ModelHairFront = reader.GetString(12);
                            //            ModelHairBack = reader.GetString(13);
                            //            ModelTail = reader.GetString(14);

                            //        }
                            //    }
                            //    Program.f1.AddingRecordsFromRTB_LogsClient(reader.GetString(1), id_BD_User);
                            //    //сериализуем информацию о пользователе в строку и отправляем клиенту
                            //    PlayerInfo playerInfo1 = new PlayerInfo();
                            //    playerInfo1.Response = new ResponsePlayerInfo();
                            //    playerInfo1.Response.IsCreatedCharacter = OP;
                            //    playerInfo1.Response.PlayerName = PlayerName;
                            //    playerInfo1.Response.Experience = Experience;
                            //    playerInfo1.Response.Type = "GetPlayerInfo";
                            //    playerInfo1.Response.VisualInformation = new VisualInformation();
                            //    playerInfo1.Response.VisualInformation.ColorBody = ColorBody;
                            //    playerInfo1.Response.VisualInformation.ColorEye = ColorEye;
                            //    playerInfo1.Response.VisualInformation.ColorHairFront1 = ColorHairFront_1;
                            //    playerInfo1.Response.VisualInformation.ColorHairFront2 = ColorHairFront_2;
                            //    playerInfo1.Response.VisualInformation.ColorHairBack1 = ColorHairBack_1;
                            //    playerInfo1.Response.VisualInformation.ColorHairBack2 = ColorHairBack_2;
                            //    playerInfo1.Response.VisualInformation.ColorTail1 = ColorTail_1;
                            //    playerInfo1.Response.VisualInformation.ColorTail2 = ColorTail_2;
                            //    playerInfo1.Response.VisualInformation.ModelHairFront = ModelHairFront;
                            //    playerInfo1.Response.VisualInformation.ModelHairBack = ModelHairBack;
                            //    playerInfo1.Response.VisualInformation.ModelTail = ModelTail;
                            //    string ser = JsonConvert.SerializeObject(playerInfo1);

                            //    Program.f1.AddingRecordsFromRTB_LogsClient("Сериализация информации о пользователе выполнена", id_BD_User);

                            //    message = ser;
                            //    server.castMessage(message, Id);
                            //}
                        }
                        break;

                    case "Set Items information":
                        using (MySqlConnection connectionSetItemsinformation = new MySqlConnection(connStr))
                        {
                            connectionSetItemsinformation.Open();
                            Program.f1.AddingRecordsFromRTB_LogsClient("Работает!", id_BD_User);
                            WeaponInfo weaponInfo = JsonConvert.DeserializeObject<WeaponInfo>(mess);
                            foreach (var items in weaponInfo.Response.ItemsInfo)
                            {
                                string sql3 = "INSERT INTO `itemsdata` (`Name`, `UnicKey`, `UserInfoId`) VALUES ('" + items.Name + "','" + items.UnicKey + "','" + id_BD_User + "')";
                                MySqlCommand sqlcom3 = new MySqlCommand(sql3, connectionSetItemsinformation);
                                sqlcom3.ExecuteNonQuery();
                            }
                        }
                        break;

                    case "Get Items information":
                        using (MySqlConnection connectionGetItemsinformation = new MySqlConnection(connStr))
                        {
                            connectionGetItemsinformation.Open();
                            WeaponInfo weaponInfo1 = new WeaponInfo();
                            string sql4 = "SELECT `Name` ,`UnicKey` FROM" +
                                " `itemsdata` WHERE UserInfoId = '" + id_BD_User + "'"; // Строка запроса
                            string sql5 = "SELECT COUNT(*) FROM `itemsdata`"; // Строка запроса

                            MySqlCommand sqlcom5 = new MySqlCommand(sql5, connectionGetItemsinformation);
                            int Count = Convert.ToInt32(sqlcom5.ExecuteScalar().ToString());
                            MySqlCommand sqlcom4 = new MySqlCommand(sql4, connectionGetItemsinformation);
                            using (MySqlDataReader reader = sqlcom4.ExecuteReader())
                            {

                                string Name = string.Empty;
                                string UnicKey = string.Empty;
                                Program.f1.AddingRecordsFromRTB_LogsClient(Count.ToString(), id_BD_User);
                                int x = 0;
                                weaponInfo1.Response = new ResponseWeaponInfo();
                                weaponInfo1.Response.ItemsInfo = new WeaponsInfo[Count];
                                weaponInfo1.Response.Type = "Get Weapon information";

                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        weaponInfo1.Response.ItemsInfo[x] = new WeaponsInfo()
                                        {
                                            Name = reader.GetString(0),
                                            UnicKey = reader.GetString(1)
                                        };
                                        Program.f1.AddingRecordsFromRTB_LogsClient(x.ToString(), id_BD_User);
                                        x++;
                                    }
                                }

                                //сериализуем информацию о пользователе в строку и отправляем клиенту


                                string ser = JsonConvert.SerializeObject(weaponInfo1);

                                Program.f1.AddingRecordsFromRTB_LogsClient("Сериализация информации об оружие выполнена", id_BD_User);

                                message = ser;
                                server.castMessage(message, Id);
                            }
                        }
                        break;

                    case "Set temporal information":

                        Program.f1.AddingRecordsFromRTB_LogsClient("Данные приняты: " + mess, id_BD_User);
                        temporal_informationJSON = mess;

                        break;

                    case "Get temporal information":

                        message = temporal_informationJSON;
                        server.castMessage(message, Id);
                        Program.f1.AddingRecordsFromRTB_LogsClient("Данные клиенту отправлены: " + message, id_BD_User);
                        break;

                    case "testReqouest":
                        Program.f1.AddingRecordsFromRTB_LogsClient("Данные приняты: " + mess, id_BD_User);
                        server.castMessage("lol", Id);
                        break;

                    case "SendRequestFriebds":
                        using (MySqlConnection connectionSendRequestFriebds = new MySqlConnection(connStr))
                        {
                            connectionSendRequestFriebds.Open();
                            int id_Friends = 0;
                            int x = 0;
                            int id_FromWhomCameTheRequest = 0;
                            SendRequestFriebds sendRequestFriebds = JsonConvert.DeserializeObject<SendRequestFriebds>(mess);

                            string PlayerNameSendReq = sendRequestFriebds.Response.FromWhomCameTheRequest;  //Тот кто отправил заявку
                            string PlayerNameReceptionReq = sendRequestFriebds.Response.PlayerName;         //тот кому пришла заявка

                            Program.f1.AddingRecordsFromRTB_LogsClient("Данные приняты ,имя того кому отправленна " +
                                "заявка: " + sendRequestFriebds.Response.PlayerName + "\nИмя того от кого пришла заявка" + sendRequestFriebds.Response.FromWhomCameTheRequest + "\nИдет поиск в базе данных", id_BD_User);
                            //Получаем ID пользователя кому отправленна заявка и от кого пришла заявка
                            string GetIdUserSendToReqiest = "SELECT `UserInfoId` FROM" +
                                " `playersinfo` WHERE NickName = '" + PlayerNameReceptionReq + "' OR NickName = '" + PlayerNameSendReq + "' ORDER BY FIELD(`NickName`, '" + PlayerNameReceptionReq + "','" + PlayerNameSendReq + "')"; // Строка запроса

                            MySqlCommand sqlcom4 = new MySqlCommand(GetIdUserSendToReqiest, connectionSendRequestFriebds);
                            using (MySqlDataReader reader = sqlcom4.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        if (x == 0)
                                        {
                                        Program.f1.AddingRecordsFromRTB_LogsClient("Id пользователя в БД" +
                                        ": " + reader.GetString(0) + "\nИдет поиск в базе данных", id_BD_User);
                                        id_Friends = Convert.ToInt32(reader.GetString(0));
                                        }
                                        if (x == 1)
                                        {
                                            id_FromWhomCameTheRequest = Convert.ToInt32(reader.GetString(0));
                                        }
                                        x++;
                                    }
                                }

                            }

                            Program.f1.AddingRecordsFromRTB_LogsClient("Цикл прошел: " + x, id_BD_User);

                            SendRequestFriebds sendRequestFriebds1 = new SendRequestFriebds();
                            sendRequestFriebds1.Response = new ResponseSendRequestFriebds();
                            sendRequestFriebds1.Response.Type = "SendRequestFriebds";
                            sendRequestFriebds1.Response.PlayerName = sendRequestFriebds.Response.FromWhomCameTheRequest;
                            sendRequestFriebds1.Response.FromWhomCameTheRequestId = id_FromWhomCameTheRequest.ToString();
                            server.castMessageById(JsonConvert.SerializeObject(sendRequestFriebds1), id_Friends.ToString());

                        }
                        break;

                    case "AcceptFriendRequest":

                        using (MySqlConnection connectionAcceptFriendRequest = new MySqlConnection(connStr))
                        {
                            connectionAcceptFriendRequest.Open();
                            AcceptFriendRequest acceptFriendRequest = JsonConvert.DeserializeObject<AcceptFriendRequest>(mess);
                            string sql3 = "INSERT INTO `friendslist` (`PlayerId`, `FriendsId`) VALUES ('" + Id + "','" + acceptFriendRequest.Response.SomeoneMadeARequest + "')";
                            string sql4 = "INSERT INTO `friendslist` (`PlayerId`, `FriendsId`) VALUES ('" + acceptFriendRequest.Response.SomeoneMadeARequest + "','" + Id + "')";
                            MySqlCommand sqlcom3 = new MySqlCommand(sql3, connectionAcceptFriendRequest);
                            MySqlCommand sqlcom4 = new MySqlCommand(sql4, connectionAcceptFriendRequest);
                            sqlcom3.ExecuteNonQuery();
                            sqlcom4.ExecuteNonQuery();
                            Program.f1.AddingRecordsFromRTB_LogsClient("Пользователь добавлен в друзья!", id_BD_User);

                            AswerFriendRequest aswerFriendRequest = new AswerFriendRequest();
                            aswerFriendRequest.Response = new ResponseAswerFriendRequest();
                            aswerFriendRequest.Response.Type = "AswerFriendRequest";
                            aswerFriendRequest.Response.Aswer = "True";
                            Program.f1.AddingRecordsFromRTB_LogsClient("ID пользователя которому был отправлен запрос о том что пользователь принял заявку: " + acceptFriendRequest.Response.SomeoneMadeARequest, id_BD_User);
                            server.castMessageById(JsonConvert.SerializeObject(aswerFriendRequest), acceptFriendRequest.Response.SomeoneMadeARequest);
                        }

                        break;
                        //Метод отправляет клиенту его список друзей
                    case "GetFriendsList":
                        using (MySqlConnection connectionGetFriendsList = new MySqlConnection(connStr))
                        {
                            connectionGetFriendsList.Open();
                            FriendsInfo friendsInfo = new FriendsInfo();
                            string sql4 = "SELECT `NickName` FROM" +
                                " `playersinfo` WHERE UserInfoId IN (SELECT `FriendsId` FROM `friendslist` WHERE PlayerId = '" + id_BD_User + "')"; // Строка запроса
                            string sql5 = "SELECT COUNT(*) FROM `friendslist` WHERE PlayerId = '" + id_BD_User + "'"; // Строка запроса

                            MySqlCommand sqlcom5 = new MySqlCommand(sql5, connectionGetFriendsList);
                            int Count = Convert.ToInt32(sqlcom5.ExecuteScalar().ToString());
                            MySqlCommand sqlcom4 = new MySqlCommand(sql4, connectionGetFriendsList);
                            using (MySqlDataReader reader = sqlcom4.ExecuteReader())
                            {
                                string Name = string.Empty;
                                int x = 0;
                                friendsInfo.Response = new ResponseFriendsInfo();
                                friendsInfo.Response.FriendsInfo = new FriendsInfoElement[Count];
                                friendsInfo.Response.Type = "GetFriendsList";

                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        friendsInfo.Response.FriendsInfo[x] = new FriendsInfoElement()
                                        {
                                            Name = reader.GetString(0)
                                        };
                                        x++;
                                    }
                                }

                                string ser = JsonConvert.SerializeObject(friendsInfo);
                                Program.f1.AddingRecordsFromRTB_LogsClient("Сериализация информации о друзьях выполнена", id_BD_User);
                                message = ser;

                                server.castMessage(message, Id);
                            }
                        }
                            break;

                        case "SetExperience":
                            using (MySqlConnection connectionSetExperience = new MySqlConnection(connStr))
                            {
                                connectionSetExperience.Open();
                            string Experience = string.Empty;
                            Program.f1.AddingRecordsFromRTB_LogsClient("Повышение ранга от Id: " + id_BD_User, id_BD_User);
                                string sql = "SELECT `Experience` FROM `playersinfo` WHERE UserInfoId = '" + id_BD_User + "'"; // Строка запроса
                                SetExperience setExperience  = JsonConvert.DeserializeObject<SetExperience>(mess);
                                MySqlCommand sqlcom4 = new MySqlCommand(sql, connectionSetExperience);
                            using (MySqlDataReader reader = sqlcom4.ExecuteReader())
                            {

                                

                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        Experience = reader.GetString(0);
                                    }
                                }
                            }
                            int NewExperience = Convert.ToInt32(Experience) + (int)setExperience.Response.ExperiencePoint;

                            string sql3 = "UPDATE `playersinfo` SET `Experience` = " + NewExperience + " WHERE UserInfoId = " + id_BD_User;
                                    //сериализуем информацию о пользователе в строку и отправляем клиенту
                                    MySqlCommand sqlcom2 = new MySqlCommand(sql3, connectionSetExperience);
                                    sqlcom2.ExecuteNonQuery();

                                    SetExperience setExperience1 = new SetExperience();
                            setExperience1.Response = new ResponseSetExperience();
                                    setExperience1.Response.Type = "GetExperience";
                                    setExperience1.Response.ExperiencePoint = Convert.ToInt32(NewExperience);
                                    JsonConvert.SerializeObject(setExperience1);

                                    message = JsonConvert.SerializeObject(setExperience1);
                                    server.castMessage(message, Id);
                                

                            }
                            break;
                        //Program.f1.AddingRecordsFromRTB_Logs(message);
                        //server.castMessage(message, this.Id);
                        //}
                        //catch
                        //{
                        //    message = String.Format("{0}: отключен", Id);
                        //    Program.f1.AddingRecordsFromRTB_LogsClient(message);
                        //    server.castMessage(message, this.Id);
                        //    break;
                        //}
                    }

            }
            //}

            //catch (Exception e)
            //{
            //    Program.f1.AddingRecordsFromRTB_LogsClient(e.Message);
            //}
            //finally
            //{
            //    // в случае выхода из цикла закрываем ресурсы
            //    server.RemoveConnection(this.Id);
            //    Close();
            //}
        }

       
        // закрытие подключения
        protected internal void Close()
        {
            if (Stream != null)
                Stream.Close();
            if (client != null)
                client.Close();
            //Program.f1.AddingRecordsFromRTB_LogsClient("Отключено от сервера", id_BD_User);
        }
    }
}
